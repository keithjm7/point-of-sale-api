/*jshint camelcase: false */

'use strict';

module.exports = function(app, config, ejwt) {

    app.use('/api/', ejwt({
            secret: config.token_secret,
            userProperty: 'tokenPayload'
        })
        .unless({
            path: [
                '/',
                '/test',
                '/logout',
                '/docs',
                config.api_version + '/modules',
                config.api_version + '/module_item',
            ]
        }));

    if (app.get('env') === 'development' || app.get('env') === 'staging') {
        app.use(function(err, req, res, next) {
            /*jshint unused: vars*/
            console.info('DEVELOPMENT / STAGING error: ');
            if (err.name === 'UnauthorizedError') {
                res.status(401).json({
                    response: {
                        result: 'UnauthorizedError',
                        success: false,
                        msg: err.inner
                    },
                    statusCode: 401
                });
            }
        });
    }



    app.use(function(err, req, res, next) {
        /*jshint unused: vars*/
        console.info('PRODUCTION error: ');
        if (err.name === 'UnauthorizedError') {
            res.status(401).json({
                response: {
                    result: 'UnauthorizedError',
                    success: false,
                    msg: err.inner
                },
                statusCode: 401
            });
        }
    });
};
