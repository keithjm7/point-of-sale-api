/*jshint camelcase: false */

'use strict';

var env = process.env.NODE_ENV;
var config = require('../config/environment/' + env);

var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash-node');

module.exports = function(passport, jwt) {

    passport.use('user', new LocalStrategy(
        function(username, password, done) {
            /*User.findOne({
                    u_email: username
                })
                .populate('userTypeID')
                .populate('contactID')
                .exec(verifyAuth(password, done));*/
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    function verifyAuth(password, done) {
        return function(err, user) {
            if (err) {
                console.log('error:', err);
                return done(err);
            }

            if (_.isEmpty(user)) {
                return done(null, {
                    msg: 'User does not exist with this email address.',
                    success: false,
                    result: ''
                });
            } else {
                if (user.u_isverified === 0) {
                    return done(null, {
                        msg: 'Your account has not been activated. Please check your mail for activation link.',
                        success: false,
                        result: ''
                    });
                } else {
                    if (!bcrypt.compareSync(password, user.u_password)) {
                        return done(null, {
                            msg: 'Invalid Username or Password',
                            success: false,
                            result: ''
                        });
                    }

                    var token = jwt.sign(user, config.token_secret, {
                        expiresInMinutes: 60 * 5
                    });

                    return done(null, {
                        msg: 'Login successfully',
                        success: true,
                        result: {
                            _id: user._id,
                            userType: {
                                _id: user.userTypeID._id,
                                type: user.userTypeID.userType
                            },
                            user: {
                                firstName: user.contactID.firstName,
                                surName: user.contactID.surName,
                                userClassID: user.contactID.userClassID,
                            },
                            email: user.u_email,
                            token: token
                        }
                    });
                }
            }

        };
    }
};
