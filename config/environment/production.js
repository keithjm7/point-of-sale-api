/*jshint camelcase: false */

'use strict';

module.exports = {
    env : 'production',
    db_host: process.env.DB_HOST || 'localhost',
    db_user: process.env.DB_USER || 'root',
    db_password: process.env.DB_USER || '',
    db_name: 'hr_pi',
    port: 4002, // PLEASE DONT REMOVE 'process.env.PORT'
    ip: process.env.IP,
    socket_port: process.env.SOCKET_PORT || 3333,
    app_name: process.env.APP_NAME || 'HRIS',
    api_host_url: process.env.API_HOST_URL || 'http://localhost:4002',
    frontend_host_url: process.env.FRONTEND_HOST_URL || 'http://raisestaging.engagisdemo.com.au',
    api_version: process.env.API_VERSION || '/api/1.0',
    mailgun_public_key: 'pubkey-eba806ce4f01ea08b5e79ca977f523bf',
    mailgun_api_key: 'key-9fc41d653fa0eed2e7b3ee5b20f1fe6a',
    mailgun_domain: 'sandboxfcbb916b2714462fafb4848ad489aedc.mailgun.org',
    token_secret: 'HRIS',
};
