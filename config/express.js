/*jshint camelcase: false */

'use strict';

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var middleware = require('../app/utils/middleware');
var expressValidator = require('express-validator');
var flash = require('connect-flash');

module.exports = function(app, config, passport, ejwt, mongoose) {

    app.use('/public', express.static(__dirname + '../../public'));
    app.set('port', config.port || process.env.APP_PORT);
    app.set('ip', config.ip);
    app.set('env', config.env);
    app.set('config', config);
    app.set('api_version', process.env.APP_VER || '/api/1.0');
    app.set('view engine', 'ejs');
    app.set('views', 'app/views/');
    app.use(morgan('dev'));
    app.use(methodOverride());
    app.use(expressValidator({
        customValidators: {
            isArray: function(value) {
                return Array.isArray(value);
            }
        }
    }));
    app.use(cookieParser());
    app.use(bodyParser.json({
        type: 'application/json',
        limit: '50mb'
    }));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(flash());
    app.use(middleware.allowCrossDomain);
    app.use(passport.initialize());
    app.use(passport.session());
};
