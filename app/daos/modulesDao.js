'use strict';

var Database = require('../../app/utils/database').Database;
var db = new Database();


exports.getAllModules = function getAllModules(next){
	var sql = 'SELECT * FROM modules;';
	db.query(sql, next);
};

exports.getAllModuleItems = function getAllModules(next){
	var sql = 'SELECT * FROM module_item;';
	db.query(sql, next);
};