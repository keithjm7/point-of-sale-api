'use strict';

var isLoggedIn = function(err, req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.user) {
        return next();
    } else {
        res.send(401).json({
            response: {
                result: 'UnauthorizedError',
                success: false,
                msg: err.inner
            },
            statusCode: 401
        });
    }
};


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept, Cache-Control');
    // next();
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
};

exports.isLoggedIn = isLoggedIn;
exports.allowCrossDomain = allowCrossDomain;