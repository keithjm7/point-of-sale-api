/*jshint camelcase: false */

'use strict';

var modulesDao = require('../daos/modulesDao');

function Modules() {
    this.modulesDao = modulesDao;
}

Modules.prototype.getAllModules = function(next) {
    modulesDao.getAllModules(function(err, data) {
        if (err) {
            next(err, null);
        }
        next(null, {
            result: data,
            msg: '',
            success: true
        });
    });
};

Modules.prototype.getAllModuleItems = function(next) {
    modulesDao.getAllModuleItems(function(err, data) {
        if (err) {
            next(err, null);
        }
        next(null, {
            result: data,
            msg: '',
            success: true
        });
    });
};

exports.Modules = Modules;
