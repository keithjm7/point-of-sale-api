'use strict';

exports.logout = function(req, res) {
    req.logout();
    res.status(200).json({
        msg: 'User successfully logout',
        success: true,
        result: null,
        statusCode: 200
    }).end();
};
