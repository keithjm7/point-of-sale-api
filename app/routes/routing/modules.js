'use strict';

var cb = require('./../../utils/callback');

var modulesCtrl = require('../../controllers/modulesController').Modules;
var modules = new modulesCtrl();


exports.getAllModules = function(req, res) {
    modules.getAllModules(cb.setupResponseCallback(res));
};

exports.getAllModuleItems = function(req,res){
	modules.getAllModuleItems(cb.setupResponseCallback(res));
};