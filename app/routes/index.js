/*jshint camelcase: false */

'use strict';

// ======================== VALIDATION ============================ //
var validatorUsers = require('../validation/user');


// ======================== ROUTING ============================ //
var users = require('./routing/users');
var modules = require('./routing/modules');



module.exports = function(app, passport, config, middleware) {

    app.route('/')
        .get(function onRequest(req,res){
            res.render('index');
        });

    // ======================== AUTHENTICATION ============================ //
    app.route(config.api_version + '/login')
        .post(validatorUsers.validateLogin, passport.authenticate('user'), function onRequest(req, res) {
            console.log('user: ', req.user);
            res.send(req.user);
        });

    app.route(config.api_version + '/logout')
        .get(users.logout);


    // ======================== Modules ============================ //
    app.route(config.api_version + '/modules')
        .get(modules.getAllModules);


    // ======================== Modules Items ============================ //
    app.route(config.api_version + '/module_item')
        .get(modules.getAllModuleItems);

};
