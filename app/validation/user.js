'use strict';

exports.validateLogin = function(req, res, next){

	req.checkBody('Email', 'Please provide your Email Address').notEmpty();
	req.checkBody('Email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
	req.checkBody('Password', 'Please provide your Password').notEmpty();
//	req.checkBody('Password', 'Password should be between 6-12 characters long.').len(6, 12);

	var errors = req.validationErrors();

    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};
